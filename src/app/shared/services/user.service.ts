import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import {Config} from '../config';
import {User} from '../models/User';
import { Router } from '@angular/router';
@Injectable({
    providedIn: 'root'
})
export class UserService  {

    private id: string;
    onUserUpdated: Subject<any>;
    users: User[];


    constructor(private http: HttpClient,private router:Router) {
        
        this.onUserUpdated = new Subject();
    }


   
    register(user) {
        return this.http.post<User>(Config.baseUrl + "/SignUp" , user);
        
    }
    login(authUser) {
        return this.http.post<User>(Config.baseUrl + "/login" ,authUser);
        
    }
    logout() {
        localStorage.removeItem('token');
        // this.router.navigate(['/']);
        this.router.navigateByUrl('http://localhost/back-office-backend/public/auth/login', {skipLocationChange: true}).then(() =>
        this.router.navigate(['auth/login']));
    }
}


