import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpClient
   

  ],
  exports: [
    CommonModule,
    FormsModule,
    
  ],
  declarations: [],
  providers: [HttpClient]
})
export class SharedModule {

}
