import {NgModule} from '@angular/core';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRouting } from './user.routing';
import { CommonModule } from '@angular/common'; 

@NgModule({
  imports: [FormsModule,
    ReactiveFormsModule,
    UserRouting,
    CommonModule
   
  ],
  declarations: [RegisterComponent, LoginComponent]
})
export class UserModule {
}