import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { User } from '../../shared/models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isLinear = false;
  data: User;
  status: any;
  message: any;
  token: any;
  constructor(private _formBuilder: FormBuilder, private userservice: UserService,
    private route: Router) { }

  ngOnInit() {

    this.form = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],

    });
  }

  login() {
    this.isLinear = true;
    const email = this.form.get('email').value;
    const password = this.form.get('password').value;
    const user = {
      email: email,
      password: password
    };
    this.userservice.login(user).subscribe((res: any) => {
      this.data = res;
      this.status = res.status;
      this.token = res.token ; 
      console.log(this.token);
      localStorage.setItem('token' , this.token);
      console.log(this.status);
      if (this.status == 1) {
        
      } else {
        this.message = res.message;

      }



    })
    this.route.navigate(['home']);

  }

}
