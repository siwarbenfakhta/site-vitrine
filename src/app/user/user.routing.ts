// Layouts
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from '../shared/guards/auth.guard';


export const routes: Routes = [

  {
    path: 'register',
    component: RegisterComponent,
    canActivate : [AuthGuard]
  }, 
  {
    path: 'login',
    component: LoginComponent,
    canActivate : [AuthGuard]
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRouting {
}
