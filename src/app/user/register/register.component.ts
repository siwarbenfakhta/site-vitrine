import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  isLinear = false;
  constructor(private _formBuilder: FormBuilder, private userservice: UserService,private route : Router
  ) { }

  ngOnInit() {

    this.form = this._formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.compose([Validators.pattern('^(?=.*[*.!@$%()&#])(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9*.!@$%()&#]+$'), Validators.required, Validators.minLength(5)])],
      passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]

    });
  }

  register() {
    this.isLinear = true;
    const nom = this.form.get('nom').value;
    const prenom = this.form.get('prenom').value;
    const email = this.form.get('email').value;
    const password = this.form.get('password').value;
    const user = {
      nom: nom,
      prenom: prenom,
      email: email,
      password: password
    };
    this.userservice.register(user).subscribe(res => {
      console.log(res);
      
    });
    this.route.navigate(['auth/login']);

  }

}
/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if (!control.parent || !control) {
    return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if (!password || !passwordConfirm) {
    return null;
  }

  if (passwordConfirm.value === '') {
    return null;
  }

  if (password.value === passwordConfirm.value) {
    return null;
  }

  return { 'passwordsNotMatching': true };



};


