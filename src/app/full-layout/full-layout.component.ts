import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Utils} from '../shared/utils';
import { UserService } from '../shared/services/user.service';
@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.css']
})
export class FullLayoutComponent implements OnInit {
  components: NavigationMain[] = [];
  storageService: any;
  visible : number=0 ; 
  token : string ;
  constructor(private userService:UserService,
    
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');
    if(this.token){
      this.visible =1 ;
    }
    this.initializeNavBar();
    this.changeActiveUrl(this.router.url);

    
  }





  initializeNavBar() {
    this.components = [
      {
        name:'Home',
        visible: true,
        url:'/home'
      },
      {name: 'CATEGORIES',
        visible: true,
        childrens: [
          {
            name: 'Shoes',
            url: '/example/list'
          },
          {
            name: 'Sports',
            url: '/example/formulaire'
          },
          {
            name: 'Health',
            url: '/example/formulaire'
          },
        ]
      },
    {
        name: 'Headers',
        visible: true,
        
        childrens: [
          {
            name: 'Ajouter un produit',
            url: '/product/list-product'
          },
          {
            name: 'Liste des produits',
            url: '/product/list-product'
          },
          ]
      },
      {
        name: 'MENUS',
        visible: true,
        childrens: [
          {
            name: 'Ajouter un produit',
            url: '/product/add-product'
          },
          {
            name: 'Liste des produits',
            url: '/product/list-product'
          },
          ]
      },
      {
        name: 'SLIDERS',
        visible: true,
        childrens: [
          {
            name: 'Ajouter un produit',
            url: '/product/add-product'
          },
          {
            name: 'Liste des produits',
            url: '/product/list-product'
          },
          ]
      },
      {
        name: 'FOOTERS',
        visible: true,
        childrens: [
          {
            name: 'Ajouter un produit',
            url: '/product/add-product'
          },
          {
            name: 'Liste des produits',
            url: '/product/list-product'
          },
          ]
      },
      {
        name: 'CUSTOM BLOCK',
        visible: true,
        childrens: [
          {
            name: 'Ajouter un produit',
            url: '/product/add-product'
          },
          {
            name: 'Liste des produits',
            url: '/product/list-product'
          },
          ]
      },
      {
        name: 'MY ACCOUNT',
        visible: true,
        childrens: [
          {
            name: 'My account',
            url: '/product/add-product'
          },
          {
            name: 'Order history',
            url: '/product/list-product'
          },
          ]
      },
      {
        name: 'INFORMATION',
        visible: true,
        childrens: [
          {
            name: 'About us',
            url: '/product/add-product'
          },
          {
            name: 'Delivery information',
            url: '/product/list-product'
          },
          ]
      },

    ];
  }
  changeActiveUrl(url: string) {
    this.components.forEach(
      component => {
        component.active = '';
        if (url.indexOf(component.url) !== -1) {
          component.active = 'active';
        }
        if (component.childrens) {
          component.childrens.forEach(
            child => {
              child.active = '';
              if (url.indexOf(child.url) !== -1) {
                child.active = 'active';
              }
            }
          );
        }
      }
    );
  }
  logout(){
    this.userService.logout() ;
    this.router.navigate(['auth/login']);
  }
}
  export class NavigationMain {
    public name: string;
    public active?: string;
    public childrens?: ChildrenNavigation[] = [];
    public url?: string;
    public visible?: boolean;
    public numberAlertes?: number;
    icon?:string ;
    
  }
  
  export class ChildrenNavigation {
    public name: string;
    public active?: string;
    public url?: string;
    public numberAlertes?: number;
  
    public action?: any;
    public hidden?: boolean;
  }


